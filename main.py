from os import path, mkdir, stat
from urllib.request import Request, urlopen
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

folder = 'C:\\Users\\Tomasz\\Downloads\\prnt.scImages\\'

s = Service(ChromeDriverManager().install())
driver = webdriver.Chrome(service=s)
driver.maximize_window()
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']


def download_set(letter):
	print(f'downloading all {len(letter) + 2}-lettered starting with {letter}')

	if not path.exists(folder + letter):
		mkdir(folder + letter)

	for l in alphabet:
		for i in alphabet:
			address = letter + l + i

			try:
				driver.get('https://prnt.sc/' + address)
				image = driver.find_element(By.ID, 'screenshot-image')

				src = image.get_attribute('src')
				print(f'({alphabet.index(l) * len(alphabet) + alphabet.index(i) + 1}/{len(alphabet) ** 2}) {src}')
				with open(folder + letter + '\\' + address + '.png', 'wb') as f:
					req = Request(src, headers={'User-Agent': 'XYZ/3.0'})
					webpage = urlopen(req, timeout=10).read()

					f.write(webpage)
			except Exception:  # no image
				print(f'no image at the link https://prnt.sc/{address}')


if __name__ == '__main__':
	set_name = '40hu'  # first letters of the section you want to download
	download_set(set_name)
